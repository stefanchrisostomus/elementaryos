execute pathogen#infect()
set number
set laststatus=2

filetype plugin indent on
syntax on

" Indentation settings
" filetype plugin indent on
" On pressing tab, insert 2 spaces
set expandtab
" show existing tab with 2 spaces width
set tabstop=2
set softtabstop=2
" when indenting with '>', use 2 spaces width
set shiftwidth=2
